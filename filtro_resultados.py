import xml.etree.ElementTree as ET
import re

# Definições
ARQUIVOS_RESULTADOS = ['resultados/2013.xml']
COMITES = ["Ciências da Computação"]

# Carrega o arquivo de resultados.
tree = ET.parse(ARQUIVOS_RESULTADOS[0])

# Itera por todos os documentos, filtrando aqueles que pertencem
# aos comitês indicados.
filtrados = []
for doc in tree.iter(tag="doc"):
	# Extrai o comite do documento usando a sintaxe de XPath.
	comite = doc.find("str[@name='descricao_comite_t']").text

	if comite in COMITES:
		filtrados.append(doc)

for doc in filtrados:
	# Extrai as informações do elemento usando a sintaxe de XPath.
	curriculo = doc.find("str[@name='curriculo_candidato_t']").text
	comite = doc.find("str[@name='descricao_comite_t']").text
	nome_canditado = doc.find("str[@name='nome_candidato_t']").text
	nome_instituicao = doc.find("str[@name='nome_instituicao_t']").text

	# Obtém apenas o número do currículo.
	m = re.search('(\d+)$', curriculo)
	numero_curriculo = m.group(0)

	# Imprime os dados do candidato aprovado.
	print(numero_curriculo + ", " + nome_canditado)
